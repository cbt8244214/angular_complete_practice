package com.example.demo.UserRepo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.UserDTO.UserDTO;

public interface UserRepo extends JpaRepository<UserDTO, Integer> {
	
	Optional<UserDTO>findByUsername(String username);

}
