package com.example.demo.config;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;

import java.security.Key;
import java.util.function.Function;

import org.springframework.stereotype.Service;

import io.jsonwebtoken.Claims;

@Service
public class JwtService {
    
	private static final String SECERT_KEY = "2646294A404D635166546A576E5A7234753778214125442A472D4B6150645267";   
	
	public String extractUsername(String token) {
		return extractClaim(token,Claims::getSubject);
	}
	
	
	public <T> T extractClaim(String token,Function<Claims,T> claimsResolver) {
		final  Claims claims = extractAllClaims( token);
		return claimsResolver.apply(claims);
	}  
	
	public String generateToken(map<String,Object>)
	
	private Claims extractAllClaims(String token) {
		return Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(token).getBody();
	}

	private Key getSignInKey() {
		byte[] KeyBytes = Decoders.BASE64.decode(SECERT_KEY);
		return Keys.hmacShaKeyFor(KeyBytes);
	}

}
