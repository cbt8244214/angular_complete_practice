package com.CBT.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.CBT.demo.AuthRequest.AuthRequest;
import com.CBT.demo.Model.User;
import com.CBT.demo.Repository.Repo;
//import com.CBT.demo.util.JwtUtil;

@RestController
public class HomeController {
	
	
	@Autowired
	private Repo repo;
//	@Autowired
//	private JwtUtil jwtUtil;
	
//	@Autowired
//	private AuthenticationManager authenticationManager;

	
	@GetMapping("/")
	public String Home() {
		return "Hello JWT!!";
	}
	
	
	@PostMapping("/credentials")
	public String credentials(@RequestBody User user) {
		repo.save(user);
		return "success";
	}
	
	
//	@PostMapping("/authenticate")
//	public String generateToken(@RequestBody AuthRequest authRequest) throws Exception {
//	try {	
//		authenticationManager.authenticate(
//				
//				new UsernamePasswordAuthenticationToken(authRequest.getUsername(),authRequest.getPassword())
//				
//				);
//	}catch(Exception e) {
//		throw new Exception("invalide username/password"); 
//	}
//	
//	return jwtUtil.generateToken(authRequest.getUsername());
//	
//		
//	}


}
