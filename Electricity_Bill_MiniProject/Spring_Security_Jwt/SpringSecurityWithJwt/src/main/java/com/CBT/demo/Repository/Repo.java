package com.CBT.demo.Repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import com.CBT.demo.Model.User;

@Repository
@EnableJpaRepositories
public interface Repo extends JpaRepository<User , Integer>  {

	User findByUsername(String username);

	

}

